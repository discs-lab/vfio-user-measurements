# Vfio user measurements

This repository contains 2 folders with the code supporting the paper "Is Bare-metal I/O Performance with User-defined Storage Drives Inside VMs Possible?" published on CHEOPS Rome 2023.

### bench_server_config
Contains the server automation and configuration, plus the XML `libvirt`-executable descriptions of the virtual machines used, and `bash` scripts to run them. E.g. `bench_server_config/ansible/roles/host/files/run_vm.sh -n vfio-user`

### latency_measurement
Is a snapshot of the following repository: https://github.com/jsrolon/latency-measurement which contains the `bpftrace` programs and AWK scripts to calculate the per-layer latency of the `vfio-user` setup.
